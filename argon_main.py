# -*- coding: utf-8 -*-
"""
ARGON SIMULATION

by: AA & CR
"""
import numpy as np
from numpy import linalg as la

# We set m = sigma = eps = 1, in order to obtain the 'real' values of the physical quantities one needs to multiply the values by the appropriate combination of m, sigma and eps, which gives the correct dimension.

##############################
######    Starters     #######
##############################

def set_pos_rand(particles, dimension, size, seed = 7):
    'Set random initial positions.'
    np.random.seed(seed)
    init_pos = (np.random.rand(particles,dimension) - 1/2)*size
    return init_pos

def full_square_latt(particles, dim, gap):
    'Generates the coordinates of a square lattice with dimension 3 (or 2) and N particles per row with a given gap between them.'
    size = particles-1
    if dim == 2:
        coord = np.array(np.meshgrid(range(particles),range(particles))).T.reshape(-1,2)
    else:
        coord = np.array(np.meshgrid(range(particles),range(particles),range(particles))).T.reshape(-1,3)
    coord = coord-size/2
    return coord*gap   

def fcc_latt(particles, edge):
    ''' Generates a fcc lattice. 
    The edge size (second closest neighbour distance) must be at least sqrt(2) for LJ potential. 
    The particles introduced are not the total but the ones you find along one edge. ''' 
    size = particles-1
    basis = 1.*np.array(np.meshgrid(range(particles),range(particles),range(particles))).T.reshape(-1,3)
    
    aux0 = 1.*np.array(np.meshgrid(range(particles),range(particles-1),range(particles-1))).T.reshape(-1,3)
    aux0[:,1] = aux0[:,1] + .5
    aux0[:,2] = aux0[:,2] + .5
    
    aux1 = 1.*np.array(np.meshgrid(range(particles-1),range(particles),range(particles-1))).T.reshape(-1,3)
    aux1[:,0] = aux1[:,0] + .5
    aux1[:,2] = aux1[:,2] + .5
    
    aux2 = 1.*np.array(np.meshgrid(range(particles-1),range(particles-1),range(particles))).T.reshape(-1,3)
    aux2[:,0] = aux2[:,0] + .5
    aux2[:,1] = aux2[:,1] + .5
    
    fcc = np.concatenate((basis, aux0, aux1, aux2), axis=0)
    fcc = fcc - size/2
    return fcc*edge

def eraser_rand(pos_matrix, erased, seed = 7):
    'Erase randomly part of the simulated particles. Complements full_square_latt.'
    total = pos_matrix.shape[0]
    if total <= erased:
        return print('You erased the entire population.')
    np.random.seed(seed)
    leftovers = pos_matrix.copy()
    for i in range(erased):
        cut = np.random.randint(total-i)
        leftovers = np.delete(leftovers, cut, 0)
    print('There are ',leftovers,' left.')
    return leftovers

def set_vel_rand(particles, dimension = 3, av_vel = 1, seed = 7):
    'Set random initial velocities for 2 and 3 dimensions.'
    np.random.seed(seed)
    init_vel = np.zeros((particles,dimension))
    v0 = np.random.rand(particles,1)*av_vel
    if dimension == 3:
        theta = np.random.rand(particles)*np.pi
        phi = np.random.rand(particles)*2*np.pi
        init_vel[:,0] = np.sin(theta)*np.cos(phi)
        init_vel[:,1] = np.sin(theta)*np.sin(phi)
        init_vel[:,2] = np.cos(theta)
        init_vel *= v0
    elif dimension == 2:
        phi = np.random.rand(particles)*2*np.pi
        init_vel[:,0] = np.cos(phi)
        init_vel[:,1] = np.sin(phi)
    return init_vel*v0

def set_vel_maxwell(particles, temperature, seed = 7):
    'Set random initial velocities according to the Maxwell-Boltzmann distribution. Only for 3 dimensions. temperature = kT in units of eps'
    np.random.seed(seed)
    init_vel = np.random.normal(loc=0, scale=np.sqrt(temperature), size=(particles,3))
    vel_center = np.sum(init_vel, axis = 0) / particles
    init_vel -= vel_center
    return init_vel

##############################
#####   Basic functions  #####
##############################

def distpairs(pos_matrix, size):
    'Find the vector between each pair of particles and its modulus.'
    auxdist = pos_matrix[:,None]-pos_matrix
    auxdist = (auxdist + size/2)%size-size/2
    auxnorm = la.norm(auxdist,axis=2)
    return auxdist, auxnorm

def kinetic(vel_matrix):
    'Calculation of the total kinetic energy of the system.'
    mod2 = vel_matrix**2
    return np.sum(mod2)/2

def ljpot(r_matrix):
    'Calculation of the total potential energy of the system.'
    r_inv = r_matrix.copy()
    r_inv[r_inv>0] = r_inv[r_inv>0]**(-1)
    r_inv6 = r_inv**6
    
    pots = 4*r_inv6*(r_inv6 - 1)
    # We divide by 2 in order to avoid doublecounting
    return np.sum(pots)/2

def ljforce(dist_matrix, r_matrix):
    'Calculation of the forces originated by Lennard-Jones potential.'
    forces = np.zeros(dist_matrix.shape)
    dimension = dist_matrix.shape[2]
    
    r_inv = r_matrix.copy()
    r_inv[r_inv>0] = r_inv[r_inv>0]**(-1)
    r_inv6 = r_inv**6
    r_inv8 = r_inv**8
    
    magn = 24*r_inv8*(2*r_inv6 - 1)
    for i in range(dimension):
        forces[:,:,i] = dist_matrix[:,:,i]*magn
    return forces

##############################
#######    Evolution   #######
##############################

def equilibration(init_pos, init_vel, size, temperature, max_steps, iterations_per_step, steps_after, delta, accuracy):
    'Equilibration of the system to reach desired temperature.'
    particles = init_pos.shape[0]
    dimension = init_pos.shape[1]

    positions = np.zeros((particles, dimension, max_steps*iterations_per_step+steps_after))
    velocities = np.zeros((particles, dimension, max_steps*iterations_per_step+steps_after))
    r_record = np.zeros((particles,particles,max_steps*iterations_per_step+steps_after))
    energies = np.zeros((max_steps*iterations_per_step+steps_after, 3))
    #min_distance = np.zeros(max_steps*iterations_per_step+steps_after)
    
    positions[:,:,0] = init_pos.copy()
    velocities[:,:,0] = init_vel.copy()
    dist, r_record[:,:,0] = distpairs(positions[:,:,0], size)
    #min_distance[0] = np.amin(r + size * np.identity(particles))
    
    energies[0,0] = kinetic(velocities[:,:,0])
    energies[0,1] = ljpot(r_record[:,:,0])
    energies[0,2] = energies[0,0] + energies[0,1]
    
    kinetic_wanted = 1.5*(particles - 1)*temperature
    p = iterations_per_step
    flag = 0
    i = 0
    while (i < max_steps) & (flag == 0):        
        positions[:,:,(i*p):((i+1)*p+1)], velocities[:,:,(i*p):((i+1)*p+1)], energies[(i*p):((i+1)*p+1),:], r_record[:,:,(i*p):((i+1)*p+1)] = evolution(positions[:,:,i*p], velocities[:,:,i*p], size, p+1, delta)
        mean_kinetic = np.mean(energies[(i*p):((i+1)*p+1),0])
        error = abs(kinetic_wanted - mean_kinetic) / kinetic_wanted
        if error > accuracy:
            scale = np.sqrt(kinetic_wanted / mean_kinetic)
            velocities[:,:,(i+1)*p] *= scale
        else:
            flag = 1
        i += 1
    positions[:,:,(i*p):(i*p + steps_after)], velocities[:,:,(i*p):(i*p + steps_after)], energies[(i*p):(i*p + steps_after),:], r_record[:,:,(i*p):(i*p + steps_after)] = evolution(positions[:,:,i*p], velocities[:,:,i*p], size, steps_after, delta)
    
    return positions[:,:,:i*p+steps_after-1], velocities[:,:,:i*p+steps_after-1], energies[:i*p+steps_after-1,:], r_record[:,:,:i*p+steps_after-1]

def evolution(init_pos, init_vel, size, iterations=10000, delta=10**(-3), method='verlet'):
    'Calculation of the evolution of the system given an initial configuration.'
    particles = init_pos.shape[0]
    dimension = init_pos.shape[1]
    
    positions = np.zeros((particles,dimension,iterations))
    velocities = np.zeros((positions.shape))
    r_record = np.zeros((particles,particles,iterations))
    energies = np.zeros((iterations,3))
    min_distance = np.zeros(iterations)
    
    positions[:,:,0] = init_pos.copy()
    velocities[:,:,0] = init_vel.copy()
    
    dist, r_record[:,:,0] = distpairs(positions[:,:,0],size)
    force0 = ljforce(dist, r_record[:,:,0])
    #min_distance[0] = np.amin(r + size*np.identity(particles))
    
    energies[0,0] = kinetic(velocities[:,:,0])
    energies[0,1] = ljpot(r_record[:,:,0])
    
    if method == 'euler':
        for i in range(iterations-1):  
            positions[:,:,i+1] = positions[:,:,i]+delta*velocities[:,:,i]
            positions[:,:,i+1] = (positions[:,:,i+1] + size/2)%size - size/2
            velocities[:,:,i+1] = velocities[:,:,i]+delta*force0.sum(axis=1)

            dist, r_record[:,:,i+1] = distpairs(positions[:,:,i+1],size)
            force0 = ljforce(dist, r_record[:,:,i+1])
            #min_distance[i+1] = np.amin(r + size*np.identity(particles))

            energies[i+1,0] = kinetic(velocities[:,:,i+1])
            energies[i+1,1] = ljpot(r_record[:,:,i+1])
            
    elif method == 'verlet':
        for i in range(iterations-1):  
            positions[:,:,i+1] = positions[:,:,i] + delta*velocities[:,:,i] + 0.5*force0.sum(axis=1)*(delta**2)
            positions[:,:,i+1] = (positions[:,:,i+1] + size/2)%size - size/2

            dist, r_record[:,:,i+1] = distpairs(positions[:,:,i+1],size)
            force1 = ljforce(dist, r_record[:,:,i+1])
            #min_distance[i+1] = np.amin(r + size*np.identity(particles))

            velocities[:,:,i+1] = velocities[:,:,i] + delta*(force0.sum(axis=1) + force1.sum(axis=1))/2

            force0 = force1.copy()

            energies[i+1,0] = kinetic(velocities[:,:,i+1])
            energies[i+1,1] = ljpot(r_record[:,:,i+1])
    else:
        print('Please introduce a valid method.')
    
    energies[:,2] = energies[:,0] + energies[:,1]
    
    return positions, velocities, energies, r_record


##############################
######   Observables    ######
##############################

def autocorrelation(observable, dt):
    '''
    Calculates the correlation function of a certain observable.
    
    It also returns the time array in order to facilitate the plotting.
    '''
    N = len(observable)
    corr = np.zeros(N)
    time = np.arange(N)*dt
    for t in range(N):
        A = observable[0:N-1-t]
        B = observable[t:N-1]
        corr[t] = np.sum(A*B) - np.sum(A)*np.sum(B)/(N-t)
        corr[t] /= N-t
    return corr, time
        
def pair_correlation(r_record, size, bins):
    'Calculation of the pair correlation function by average over the simulation time.'
    particles = r_record.shape[0]
    time = r_record.shape[2]
    bin_size = size/bins
    prefactor = size**3 / (2*np.pi*bin_size*particles*(particles-1))
    bin_edges = np.linspace(0, bin_size*bins, bins+1)
    distance = np.linspace(0, bin_size*(bins-1), bins) + bin_size/2
    pairs = r_record.flatten()
    pairs = pairs[pairs>0]
    n = np.histogram(pairs, bin_edges)[0] / 2
    pair_corr = prefactor*n/(time*distance**2)

    return pair_corr, distance

##############################
#####   Handy function   #####
##############################

def normal_autocorr(mu, sigma, tau, N):
    """Generates an autocorrelated sequence of Gaussian random numbers.
    
    Each of the random numbers in the sequence of length `N` is distributed
    according to a Gaussian with mean `mu` and standard deviation `sigma` (just
    as in `numpy.random.normal`, with `loc=mu` and `scale=sigma`). Subsequent
    random numbers are correlated such that the autocorrelation function
    is on average `exp(-n/tau)` where `n` is the distance between random
    numbers in the sequence.
    
    This function implements the algorithm described in
    https://www.cmu.edu/biolphys/deserno/pdf/corr_gaussian_random.pdf
    
    Parameters
    ----------
    
    mu: float
        mean of each Gaussian random number
    sigma: float
        standard deviation of each Gaussian random number
    tau: float
        autocorrelation time
    N: int
        number of desired random numbers
    
    Returns:
    --------
    sequence: numpy array
        array of autocorrelated random numbers
    """
    f = np.exp(-1./tau)
    
    sequence = np.zeros(shape=(N,))
    
    sequence[0] = np.random.normal(0, 1)
    for i in range(1, N):
        sequence[i] = f * sequence[i-1] + np.sqrt(1 - f**2) * np.random.normal(0, 1)
    
    return mu + sigma * sequence    